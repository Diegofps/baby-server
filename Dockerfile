# Use an official Python runtime as a parent image
FROM ruby:2.7.1-slim
 
# Set the working directory to /project
WORKDIR /project

COPY src .
 
# Use apt in non interactive mode if you need to install a package
RUN apt update && DEBIAN_FRONTEND=noninteractive && apt -yq install ruby locales
 
# To set the language of the system, this is important when you system need special characters
RUN locale-gen pt_BR.UTF-8
ENV LANG="pt_BR.UTF-8"
 
# Install any needed packages specified in requirements.txt
RUN gem install sinatra --no-document
 
# Make port 80 available to the world outside this container
EXPOSE 4567
 
# Define environment variable
ENV NAME World
 
# Run app.py when the container launches
CMD ["ruby", "main.rb"]
