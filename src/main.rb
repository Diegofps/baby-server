require 'sinatra'

set :bind, '0.0.0.0'
set :port, 4567

get '/home' do
  "The baby is conscious in #{ENV["HOSTNAME"]} (#{RUBY_PLATFORM})!\n"
end
